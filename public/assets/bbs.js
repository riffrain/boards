"use strict";



define('bbs/adapters/application', ['exports', 'ember-data'], function (exports, _emberData) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _emberData.default.RESTAdapter.extend({
    namespace: 'api',
    host: 'https://bbs.o10e.org'
  });
});
define('bbs/app', ['exports', 'bbs/resolver', 'ember-load-initializers', 'bbs/config/environment'], function (exports, _resolver, _emberLoadInitializers, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  var App = Ember.Application.extend({
    modulePrefix: _environment.default.modulePrefix,
    podModulePrefix: _environment.default.podModulePrefix,
    Resolver: _resolver.default
  });

  (0, _emberLoadInitializers.default)(App, _environment.default.modulePrefix);

  exports.default = App;
});
define('bbs/components/header-menus', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Component.extend({});
});
define('bbs/components/post-form', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Component.extend({
    sendPost: Ember.inject.service('send-post'),

    enableTitle: Ember.computed('type', function () {
      if (this.get('type') === 'post') {
        return true;
      } else {
        return false;
      }
    }),

    getUrl: function getUrl() {
      if (this.get('type') === 'reply') {
        return '/api/posts/add/' + this.get('postId');
      } else {
        return '/api/posts/add/';
      }
    },

    beforeSubmit: function beforeSubmit() {
      this.set('isPosting', true);
    },
    afterSubmit: function afterSubmit() {
      this.set('isPosting', false);

      this.set('title', null);
      this.set('name', null);
      this.set('email', null);
      this.set('body', null);
    },
    completeSubmit: function completeSubmit(data) {
      // nothing
    },

    actions: {
      onSubmit: function onSubmit() {
        var self = this;
        var data = self.getProperties('name', 'email', 'body');

        if (this.get('type') !== 'reply') {
          data.title = this.get('title');
        }
        if (this.get('replyId')) {
          data.destination_id = this.get('replyId');
        }
        var url = this.getUrl();

        self.beforeSubmit();

        self.get('sendPost.post')(url, data).then(function (data) {
          self.afterSubmit();
          return data;
        }).then(function (data) {
          self.completeSubmit(data);
        });
      }
    }
  });
});
define('bbs/components/post-item', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Component.extend({
    isReply: false,

    name: function () {
      if (!this.get('item.name')) {
        return 'Anonymous';
      } else {
        return this.get('item.name');
      }
    }.property('item'),

    padId: function () {
      return (this.get('item.id') || '').padStart(10, '0');
    }.property('item'),

    actions: {
      reply: function reply() {
        this.toggleProperty('isReply');
      },
      completeSend: function completeSend(data) {
        if (data.error) {
          alert(data.error.body);
        }
        this.get('modelReload')();
      }
    }
  });
});
define('bbs/controllers/application', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Controller.extend({
    actions: {}
  });
});
define('bbs/controllers/bbs/index', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Controller.extend({
    actions: {
      completeSend: function completeSend(data) {
        if (data.error) {
          var message = '';
          Object.keys(data.error).forEach(function (key) {
            message += data.error[key] + '\n';
          });
          alert(message);
        } else {
          this.transitionToRoute('bbs.read', data.id);
        }
      }
    }
  });
});
define('bbs/controllers/bbs/read', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  var EmberObject = Ember.Object;
  var computed = Ember.computed;
  exports.default = Ember.Controller.extend({
    sendPost: Ember.inject.service('send-post'),

    actions: {
      reload: function reload() {
        return this.send('modelReload');
      },
      completeSend: function completeSend(data) {
        if (data.error) {
          alert(data.error.body);
        }
        this.send('reload');
      }
    }
  });
});
define('bbs/helpers/app-version', ['exports', 'bbs/config/environment', 'ember-cli-app-version/utils/regexp'], function (exports, _environment, _regexp) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.appVersion = appVersion;
  var version = _environment.default.APP.version;
  function appVersion(_) {
    var hash = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    if (hash.hideSha) {
      return version.match(_regexp.versionRegExp)[0];
    }

    if (hash.hideVersion) {
      return version.match(_regexp.shaRegExp)[0];
    }

    return version;
  }

  exports.default = Ember.Helper.helper(appVersion);
});
define('bbs/helpers/format-date', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.formatDate = formatDate;
  function formatDate(params, _ref) {
    var date = _ref.date,
        format = _ref.format;

    date = new Date(date);
    if (!format) format = 'YYYY-MM-DD hh:mm:ss.SSS';
    format = format.replace(/YYYY/g, date.getFullYear());
    format = format.replace(/MM/g, ('0' + (date.getMonth() + 1)).slice(-2));
    format = format.replace(/DD/g, ('0' + date.getDate()).slice(-2));
    format = format.replace(/hh/g, ('0' + date.getHours()).slice(-2));
    format = format.replace(/mm/g, ('0' + date.getMinutes()).slice(-2));
    format = format.replace(/ss/g, ('0' + date.getSeconds()).slice(-2));
    if (format.match(/S/g)) {
      var milliSeconds = ('00' + date.getMilliseconds()).slice(-3);
      var length = format.match(/S/g).length;
      for (var i = 0; i < length; i++) {
        format = format.replace(/S/, milliSeconds.substring(i, i + 1));
      }
    }
    return format;
  }

  exports.default = Ember.Helper.helper(formatDate);
});
define('bbs/helpers/h', ['exports', 'bbs/utils/sanitize'], function (exports, _sanitize) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.h = h;


  // google-caja sanitizer wrapper
  function h(params) {
    return _sanitize.default.sanitize(params[0]).replace(/\r?\n/g, '<br>');
  }

  exports.default = Ember.Helper.helper(h);
});
define('bbs/helpers/pluralize', ['exports', 'ember-inflector/lib/helpers/pluralize'], function (exports, _pluralize) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _pluralize.default;
});
define('bbs/helpers/singularize', ['exports', 'ember-inflector/lib/helpers/singularize'], function (exports, _singularize) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _singularize.default;
});
define('bbs/initializers/app-version', ['exports', 'ember-cli-app-version/initializer-factory', 'bbs/config/environment'], function (exports, _initializerFactory, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  var _config$APP = _environment.default.APP,
      name = _config$APP.name,
      version = _config$APP.version;
  exports.default = {
    name: 'App Version',
    initialize: (0, _initializerFactory.default)(name, version)
  };
});
define('bbs/initializers/container-debug-adapter', ['exports', 'ember-resolver/resolvers/classic/container-debug-adapter'], function (exports, _containerDebugAdapter) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'container-debug-adapter',

    initialize: function initialize() {
      var app = arguments[1] || arguments[0];

      app.register('container-debug-adapter:main', _containerDebugAdapter.default);
      app.inject('container-debug-adapter:main', 'namespace', 'application:main');
    }
  };
});
define('bbs/initializers/data-adapter', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'data-adapter',
    before: 'store',
    initialize: function initialize() {}
  };
});
define('bbs/initializers/ember-data', ['exports', 'ember-data/setup-container', 'ember-data'], function (exports, _setupContainer) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'ember-data',
    initialize: _setupContainer.default
  };
});
define('bbs/initializers/export-application-global', ['exports', 'bbs/config/environment'], function (exports, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.initialize = initialize;
  function initialize() {
    var application = arguments[1] || arguments[0];
    if (_environment.default.exportApplicationGlobal !== false) {
      var theGlobal;
      if (typeof window !== 'undefined') {
        theGlobal = window;
      } else if (typeof global !== 'undefined') {
        theGlobal = global;
      } else if (typeof self !== 'undefined') {
        theGlobal = self;
      } else {
        // no reasonable global, just bail
        return;
      }

      var value = _environment.default.exportApplicationGlobal;
      var globalName;

      if (typeof value === 'string') {
        globalName = value;
      } else {
        globalName = Ember.String.classify(_environment.default.modulePrefix);
      }

      if (!theGlobal[globalName]) {
        theGlobal[globalName] = application;

        application.reopen({
          willDestroy: function willDestroy() {
            this._super.apply(this, arguments);
            delete theGlobal[globalName];
          }
        });
      }
    }
  }

  exports.default = {
    name: 'export-application-global',

    initialize: initialize
  };
});
define('bbs/initializers/injectStore', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'injectStore',
    before: 'store',
    initialize: function initialize() {}
  };
});
define('bbs/initializers/store', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'store',
    after: 'ember-data',
    initialize: function initialize() {}
  };
});
define('bbs/initializers/transforms', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'transforms',
    before: 'store',
    initialize: function initialize() {}
  };
});
define("bbs/instance-initializers/ember-data", ["exports", "ember-data/instance-initializers/initialize-store-service"], function (exports, _initializeStoreService) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: "ember-data",
    initialize: _initializeStoreService.default
  };
});
define('bbs/models/post', ['exports', 'ember-data'], function (exports, _emberData) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _emberData.default.Model.extend({
    title: _emberData.default.attr(),
    name: _emberData.default.attr(),
    email: _emberData.default.attr(),
    body: _emberData.default.attr(),
    created_at: _emberData.default.attr(),
    replies: _emberData.default.hasMany('reply')
  });
});
define('bbs/models/reply', ['exports', 'ember-data'], function (exports, _emberData) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _emberData.default.Model.extend({
    name: _emberData.default.attr(),
    email: _emberData.default.attr(),
    body: _emberData.default.attr(),
    post_id: _emberData.default.attr(),
    destination_id: _emberData.default.attr(),
    created_at: _emberData.default.attr()
  });
});
define('bbs/resolver', ['exports', 'ember-resolver'], function (exports, _emberResolver) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _emberResolver.default;
});
define('bbs/router', ['exports', 'bbs/config/environment'], function (exports, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  var Router = Ember.Router.extend({
    location: _environment.default.locationType,
    rootURL: _environment.default.rootURL
  });

  Router.map(function () {
    this.route('not-found', { path: '/*path' });
    this.route('index', { path: '/' });
    this.route('bbs', function () {
      this.route('index', { path: '/' });
      this.route('read', { path: '/read/:id' });
    });
  });

  exports.default = Router;
});
define('bbs/routes/application', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Route.extend({
    tagName: 'div'
  });
});
define('bbs/routes/bbs/index', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Route.extend({
    model: function model() {
      //return this.get('store').query('post', {size: 20});
      return this.get('store').findAll('post');
    },
    setupController: function setupController(controller, model) {
      this._super.apply(this, arguments);
    },

    actions: {
      modelReload: function modelReload() {
        this.refresh();
      }
    }
  });
});
define('bbs/routes/bbs/read', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Route.extend({
    model: function model(params) {
      this.set('postId', params.id);
      return this.get('store').find('post', params.id);
    },
    setupController: function setupController(controller, model) {
      this._super.apply(this, arguments);
      controller.set('postId', this.get('postId'));
    },

    actions: {
      modelReload: function modelReload() {
        this.refresh();
      }
    }
  });
});
define('bbs/routes/index', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Route.extend({});
});
define('bbs/routes/posts', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Route.extend({});
});
define('bbs/serializers/post', ['exports', 'ember-data'], function (exports, _emberData) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _emberData.default.RESTSerializer.extend(_emberData.default.EmbeddedRecordsMixin, {
    attrs: {
      replies: { embedded: 'always' }
    }
  });
});
define('bbs/services/ajax', ['exports', 'ember-ajax/services/ajax'], function (exports, _ajax) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _ajax.default;
    }
  });
});
define('bbs/services/flash-messages.js', ['exports', 'ember-cli-flash/flash/object'], function (exports, _object) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  var computed = Ember.computed,
      get = Ember.get,
      run = Ember.run;
  exports.default = Ember.Service.extend({
    queue: Ember.A([]),
    isEmpty: computed.equal('queue.length', 0),

    defaultTimeout: 2000,

    success: function success(message) {
      var timeout = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : get(this, 'defaultTimeout');

      this._add(message, 'success', timeout);
    },
    info: function info(message) {
      var timeout = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : get(this, 'defaultTimeout');

      this._add(message, 'info', timeout);
    },
    warning: function warning(message) {
      var timeout = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : get(this, 'defaultTimeout');

      this._add(message, 'warning', timeout);
    },
    danger: function danger(message) {
      var timeout = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : get(this, 'defaultTimeout');

      this._add(message, 'danger', timeout);
    },
    addMessage: function addMessage(message) {
      var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'info';
      var timeout = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : get(this, 'defaultTimeout');

      this._add(message, type, timeout);
    },
    clearMessages: function clearMessages() {
      var flashes = get(this, 'queue');

      run.next(this, function () {
        flashes.clear();
      });
    },


    // private
    _add: function _add(message, type, timeout) {
      var flashes = get(this, 'queue');
      var flash = this._newFlashMessage(this, message, type, timeout);

      run.next(this, function () {
        flashes.pushObject(flash);
      });
    },
    _newFlashMessage: function _newFlashMessage(service, message, type, timeout) {
      Ember.assert('Must pass a valid flash service', service);
      Ember.assert('Must pass a valid flash message', message);

      type = typeof type === 'undefined' ? 'info' : type;
      timeout = timeout || Ember.get(this, 'defaultTimeout');

      return _object.default.create({
        type: type,
        message: message,
        timeout: timeout,
        flashService: service
      });
    }
  });
});
define('bbs/services/send-post', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Service.extend({
    post: function post(url, data) {
      return new Promise(function (resolve, reject) {
        fetch(url, {
          method: 'POST',
          headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(data)
        }).then(function (res) {
          return res.json();
        }).then(function (json) {
          resolve(json);
        }).catch(function (error) {
          reject(error);
        });
      });
    }
  });
});
define("bbs/templates/application", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "rjQpiLtg", "block": "{\"symbols\":[],\"statements\":[[1,[18,\"header-menus\"],false],[0,\"\\n\\n\"],[6,\"div\"],[7],[0,\"\\n  \"],[1,[18,\"outlet\"],false],[0,\"\\n\"],[8]],\"hasEval\":false}", "meta": { "moduleName": "bbs/templates/application.hbs" } });
});
define("bbs/templates/bbs/index", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "Z9saTd5n", "block": "{\"symbols\":[\"post\"],\"statements\":[[1,[25,\"post-form\",null,[[\"url\",\"type\",\"completeSubmit\"],[\"/api/posts/add\",\"post\",[25,\"action\",[[19,0,[]],\"completeSend\"],null]]]],false],[0,\"\\n\\n\"],[6,\"div\"],[7],[0,\"\\n\"],[4,\"each\",[[19,0,[\"model\"]]],null,{\"statements\":[[0,\"    \"],[6,\"div\"],[7],[0,\"\\n\"],[4,\"link-to\",[\"bbs.read\",[19,1,[\"id\"]]],null,{\"statements\":[[0,\"        \"],[1,[19,1,[\"id\"]],false],[0,\" - \"],[1,[19,1,[\"title\"]],false],[0,\"\\n\"]],\"parameters\":[]},null],[0,\"    \"],[8],[0,\"\\n\"]],\"parameters\":[1]},null],[8]],\"hasEval\":false}", "meta": { "moduleName": "bbs/templates/bbs/index.hbs" } });
});
define("bbs/templates/bbs/read", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "hDpQadTz", "block": "{\"symbols\":[\"item\"],\"statements\":[[6,\"h2\"],[7],[1,[20,[\"model\",\"title\"]],false],[8],[0,\"\\n\"],[1,[25,\"post-item\",null,[[\"item\",\"isDisableReply\"],[[19,0,[\"model\"]],true]]],false],[0,\"\\n\\n\"],[4,\"each\",[[19,0,[\"model\",\"replies\"]]],null,{\"statements\":[[0,\"  \"],[1,[25,\"post-item\",null,[[\"item\",\"modelReload\"],[[19,1,[]],[25,\"action\",[[19,0,[]],\"reload\"],null]]]],false],[0,\"\\n\"]],\"parameters\":[1]},null],[0,\"\\n\"],[1,[25,\"post-form\",null,[[\"type\",\"postId\",\"completeSubmit\"],[\"reply\",[19,0,[\"model\",\"id\"]],[25,\"action\",[[19,0,[]],\"completeSend\"],null]]]],false],[0,\"\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "bbs/templates/bbs/read.hbs" } });
});
define("bbs/templates/components/header-menus", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "UCp6ytIK", "block": "{\"symbols\":[],\"statements\":[[6,\"div\"],[9,\"class\",\"header-content\"],[7],[0,\"\\n  \"],[6,\"ul\"],[9,\"class\",\"list-unstyled nav-bar\"],[7],[0,\"\\n    \"],[6,\"li\"],[7],[0,\"\\n      \"],[4,\"link-to\",[\"index\"],null,{\"statements\":[[0,\"トップ\"]],\"parameters\":[]},null],[0,\"\\n    \"],[8],[0,\"\\n    \"],[6,\"li\"],[7],[0,\"\\n      \"],[4,\"link-to\",[\"bbs.index\"],null,{\"statements\":[[0,\"一覧\"]],\"parameters\":[]},null],[0,\"\\n    \"],[8],[0,\"\\n  \"],[8],[0,\"\\n\"],[8]],\"hasEval\":false}", "meta": { "moduleName": "bbs/templates/components/header-menus.hbs" } });
});
define("bbs/templates/components/post-form", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "/xPtVnws", "block": "{\"symbols\":[],\"statements\":[[6,\"div\"],[9,\"style\",\"width: 240px;\"],[7],[0,\"\\n  \"],[6,\"form\"],[9,\"class\",\"ui mini form\"],[3,\"action\",[[19,0,[]],\"onSubmit\"],[[\"on\"],[\"submit\"]]],[7],[0,\"\\n    \"],[6,\"div\"],[9,\"class\",\"field\"],[7],[0,\"\\n      \"],[6,\"button\"],[9,\"class\",\"ui mini button\"],[10,\"disabled\",[18,\"isPosting\"],null],[7],[0,\"投稿\"],[8],[0,\"\\n    \"],[8],[0,\"\\n\"],[4,\"if\",[[19,0,[\"enableTitle\"]]],null,{\"statements\":[[0,\"      \"],[6,\"div\"],[9,\"class\",\"field\"],[7],[0,\"\\n        \"],[6,\"div\"],[7],[0,\"\\n          \"],[1,[25,\"input\",null,[[\"type\",\"placeholder\",\"value\"],[\"text\",\"タイトル\",[19,0,[\"title\"]]]]],false],[0,\"\\n        \"],[8],[0,\"\\n      \"],[8],[0,\"\\n\"]],\"parameters\":[]},null],[0,\"    \"],[6,\"div\"],[9,\"class\",\"two fields\"],[7],[0,\"\\n      \"],[6,\"div\"],[9,\"class\",\"field\"],[7],[0,\"\\n        \"],[1,[25,\"input\",null,[[\"type\",\"placeholder\",\"value\"],[\"text\",\"名前\",[19,0,[\"name\"]]]]],false],[0,\"\\n      \"],[8],[0,\"\\n      \"],[6,\"div\"],[9,\"class\",\"field\"],[7],[0,\"\\n        \"],[1,[25,\"input\",null,[[\"type\",\"placeholder\",\"value\"],[\"text\",\"Email\",[19,0,[\"email\"]]]]],false],[0,\"\\n      \"],[8],[0,\"\\n    \"],[8],[0,\"\\n    \"],[6,\"div\"],[7],[0,\"\\n      \"],[6,\"div\"],[9,\"class\",\"field\"],[7],[0,\"\\n        \"],[1,[25,\"textarea\",null,[[\"value\"],[[19,0,[\"body\"]]]]],false],[0,\"\\n      \"],[8],[0,\"\\n    \"],[8],[0,\"\\n  \"],[8],[0,\"\\n\"],[8]],\"hasEval\":false}", "meta": { "moduleName": "bbs/templates/components/post-form.hbs" } });
});
define("bbs/templates/components/post-item", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "AY0+OooT", "block": "{\"symbols\":[],\"statements\":[[6,\"div\"],[9,\"class\",\"reply-content\"],[7],[0,\"\\n  \"],[6,\"div\"],[9,\"class\",\"reply-header\"],[7],[0,\"\\n    \"],[6,\"span\"],[9,\"class\",\"name\"],[7],[0,\"\\n\"],[4,\"if\",[[19,0,[\"item\",\"email\"]]],null,{\"statements\":[[0,\"        \"],[6,\"a\"],[10,\"href\",[26,[\"mailto:\",[20,[\"item\",\"email\"]]]]],[7],[1,[18,\"name\"],false],[8],[0,\"\\n\"]],\"parameters\":[]},{\"statements\":[[0,\"        \"],[1,[18,\"name\"],false],[0,\"\\n\"]],\"parameters\":[]}],[0,\"    \"],[8],[0,\"\\n    \"],[6,\"span\"],[9,\"class\",\"date\"],[7],[0,\"\\n      \"],[1,[25,\"format-date\",null,[[\"date\",\"format\"],[[19,0,[\"item\",\"created_at\"]],\"YYYY/MM/DD hh:mm\"]]],false],[0,\"\\n    \"],[8],[0,\"\\n    \"],[6,\"span\"],[9,\"class\",\"id\"],[7],[0,\"\\n      No.\"],[1,[18,\"padId\"],false],[0,\"\\n    \"],[8],[0,\"\\n  \"],[8],[0,\"\\n  \"],[6,\"div\"],[9,\"class\",\"reply-body\"],[7],[0,\"\\n    \"],[1,[25,\"h\",[[19,0,[\"item\",\"body\"]]],null],true],[0,\"\\n  \"],[8],[0,\"\\n  \"],[6,\"div\"],[9,\"class\",\"reply-footer\"],[7],[0,\"\\n\"],[4,\"unless\",[[19,0,[\"isDisableReply\"]]],null,{\"statements\":[[0,\"      \"],[6,\"div\"],[9,\"class\",\"actions\"],[7],[0,\"\\n        \"],[6,\"a\"],[9,\"class\",\"reply\"],[3,\"action\",[[19,0,[]],\"reply\",[19,0,[\"item\",\"id\"]]],[[\"on\"],[\"click\"]]],[7],[0,\"Reply\"],[8],[0,\"\\n      \"],[8],[0,\"\\n\"],[4,\"if\",[[19,0,[\"isReply\"]]],null,{\"statements\":[[0,\"        \"],[1,[25,\"post-form\",null,[[\"type\",\"completeSubmit\",\"replyId\",\"postId\"],[\"reply\",[25,\"action\",[[19,0,[]],\"completeSend\"],null],[19,0,[\"item\",\"id\"]],[19,0,[\"item\",\"post_id\"]]]]],false],[0,\"\\n\"]],\"parameters\":[]},null]],\"parameters\":[]},null],[0,\"  \"],[8],[0,\"\\n\"],[8]],\"hasEval\":false}", "meta": { "moduleName": "bbs/templates/components/post-item.hbs" } });
});
define("bbs/templates/index", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "/Bu8PQCn", "block": "{\"symbols\":[],\"statements\":[[4,\"link-to\",[\"bbs.index\"],null,{\"statements\":[[0,\"  BBS\\n\"]],\"parameters\":[]},null]],\"hasEval\":false}", "meta": { "moduleName": "bbs/templates/index.hbs" } });
});
define("bbs/templates/not-found", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "WCesYPyg", "block": "{\"symbols\":[],\"statements\":[[6,\"p\"],[7],[0,\"Oops, the page you're looking for wasn't found\"],[8],[0,\"\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "bbs/templates/not-found.hbs" } });
});
define("bbs/templates/posts", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "pxLj9Q+z", "block": "{\"symbols\":[],\"statements\":[[1,[18,\"outlet\"],false]],\"hasEval\":false}", "meta": { "moduleName": "bbs/templates/posts.hbs" } });
});
define('bbs/utils/sanitize', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {

    url: function url(_url) {
      _url = _url.toString().replace(/['"]+/g, '');
      if (/^https?:\/\//.test(_url) || /^\//.test(_url)) {
        return _url;
      }
    },

    id: function id(_id) {
      return _id;
    },

    sanitize: function sanitize(text) {
      // use google-caja
      return html_sanitize(text, this.url, this.id);
    }
  };
});


define('bbs/config/environment', ['ember'], function(Ember) {
  var prefix = 'bbs';
try {
  var metaName = prefix + '/config/environment';
  var rawConfig = document.querySelector('meta[name="' + metaName + '"]').getAttribute('content');
  var config = JSON.parse(unescape(rawConfig));

  var exports = { 'default': config };

  Object.defineProperty(exports, '__esModule', { value: true });

  return exports;
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

});

if (!runningTests) {
  require("bbs/app")["default"].create({"name":"bbs","version":"0.0.0+6a9a7a51"});
}
//# sourceMappingURL=bbs.map
