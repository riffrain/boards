'use strict';

define('bbs/tests/app.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | app');

  QUnit.test('adapters/application.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'adapters/application.js should pass ESLint\n\n');
  });

  QUnit.test('app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'app.js should pass ESLint\n\n');
  });

  QUnit.test('components/header-menus.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/header-menus.js should pass ESLint\n\n');
  });

  QUnit.test('components/post-form.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'components/post-form.js should pass ESLint\n\n34:29 - \'data\' is defined but never used. (no-unused-vars)');
  });

  QUnit.test('components/post-item.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/post-item.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/application.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/application.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/bbs/index.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/bbs/index.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/bbs/read.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'controllers/bbs/read.js should pass ESLint\n\n2:8 - \'EmberObject\' is defined but never used. (no-unused-vars)\n2:23 - \'computed\' is defined but never used. (no-unused-vars)');
  });

  QUnit.test('helpers/format-date.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/format-date.js should pass ESLint\n\n');
  });

  QUnit.test('helpers/h.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/h.js should pass ESLint\n\n');
  });

  QUnit.test('models/post.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/post.js should pass ESLint\n\n');
  });

  QUnit.test('models/reply.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/reply.js should pass ESLint\n\n');
  });

  QUnit.test('resolver.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'resolver.js should pass ESLint\n\n');
  });

  QUnit.test('router.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'router.js should pass ESLint\n\n');
  });

  QUnit.test('routes/application.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/application.js should pass ESLint\n\n');
  });

  QUnit.test('routes/bbs/index.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'routes/bbs/index.js should pass ESLint\n\n8:31 - \'model\' is defined but never used. (no-unused-vars)');
  });

  QUnit.test('routes/bbs/read.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'routes/bbs/read.js should pass ESLint\n\n8:31 - \'model\' is defined but never used. (no-unused-vars)');
  });

  QUnit.test('routes/index.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/index.js should pass ESLint\n\n');
  });

  QUnit.test('routes/posts.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/posts.js should pass ESLint\n\n');
  });

  QUnit.test('serializers/post.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'serializers/post.js should pass ESLint\n\n');
  });

  QUnit.test('services/flash-messages.js.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'services/flash-messages.js.js should pass ESLint\n\n');
  });

  QUnit.test('services/send-post.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'services/send-post.js should pass ESLint\n\n5:16 - \'Promise\' is not defined. (no-undef)');
  });

  QUnit.test('utils/sanitize.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'utils/sanitize.js should pass ESLint\n\n16:12 - \'html_sanitize\' is not defined. (no-undef)');
  });
});
define('bbs/tests/helpers/destroy-app', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = destroyApp;
  function destroyApp(application) {
    Ember.run(application, 'destroy');
  }
});
define('bbs/tests/helpers/flash-message', ['ember-cli-flash/flash/object'], function (_object) {
  'use strict';

  _object.default.reopen({
    init: function init() {}
  });
});
define('bbs/tests/helpers/module-for-acceptance', ['exports', 'qunit', 'bbs/tests/helpers/start-app', 'bbs/tests/helpers/destroy-app'], function (exports, _qunit, _startApp, _destroyApp) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  exports.default = function (name) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    (0, _qunit.module)(name, {
      beforeEach: function beforeEach() {
        this.application = (0, _startApp.default)();

        if (options.beforeEach) {
          return options.beforeEach.apply(this, arguments);
        }
      },
      afterEach: function afterEach() {
        var _this = this;

        var afterEach = options.afterEach && options.afterEach.apply(this, arguments);
        return resolve(afterEach).then(function () {
          return (0, _destroyApp.default)(_this.application);
        });
      }
    });
  };

  var resolve = Ember.RSVP.resolve;
});
define('bbs/tests/helpers/resolver', ['exports', 'bbs/resolver', 'bbs/config/environment'], function (exports, _resolver, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  var resolver = _resolver.default.create();

  resolver.namespace = {
    modulePrefix: _environment.default.modulePrefix,
    podModulePrefix: _environment.default.podModulePrefix
  };

  exports.default = resolver;
});
define('bbs/tests/helpers/start-app', ['exports', 'bbs/app', 'bbs/config/environment'], function (exports, _app, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = startApp;
  function startApp(attrs) {
    var attributes = Ember.merge({}, _environment.default.APP);
    attributes = Ember.merge(attributes, attrs); // use defaults, but you can override;

    return Ember.run(function () {
      var application = _app.default.create(attributes);
      application.setupForTesting();
      application.injectTestHelpers();
      return application;
    });
  }
});
define('bbs/tests/integration/components/avatar-identicon-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleForComponent)('avatar-identicon', 'Integration | Component | avatar identicon', {
    integration: true
  });

  (0, _emberQunit.test)('it renders', function (assert) {

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template({
      "id": "a/0gCV6C",
      "block": "{\"symbols\":[],\"statements\":[[1,[18,\"avatar-identicon\"],false]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template({
      "id": "5hQEHPVD",
      "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[4,\"avatar-identicon\",null,null,{\"statements\":[[0,\"      template block text\\n\"]],\"parameters\":[]},null],[0,\"  \"]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), 'template block text');
  });
});
define('bbs/tests/integration/components/chat-window-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleForComponent)('chat-window', 'Integration | Component | chat window', {
    integration: true
  });

  (0, _emberQunit.test)('it renders', function (assert) {

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template({
      "id": "GqVXG4pX",
      "block": "{\"symbols\":[],\"statements\":[[1,[18,\"chat-window\"],false]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template({
      "id": "0xpsEchz",
      "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[4,\"chat-window\",null,null,{\"statements\":[[0,\"      template block text\\n\"]],\"parameters\":[]},null],[0,\"  \"]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), 'template block text');
  });
});
define('bbs/tests/integration/components/header-menus-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleForComponent)('header-menus', 'Integration | Component | header menus', {
    integration: true
  });

  (0, _emberQunit.test)('it renders', function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template({
      "id": "+T0wsTvR",
      "block": "{\"symbols\":[],\"statements\":[[1,[18,\"header-menus\"],false]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template({
      "id": "Ix2Qi28h",
      "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[4,\"header-menus\",null,null,{\"statements\":[[0,\"      template block text\\n\"]],\"parameters\":[]},null],[0,\"  \"]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), 'template block text');
  });
});
define('bbs/tests/integration/components/infinite-scroller-extend-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleForComponent)('infinite-scroller-extend', 'Integration | Component | infinite scroller extend', {
    integration: true
  });

  (0, _emberQunit.test)('it renders', function (assert) {

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template({
      "id": "DuRtpa3k",
      "block": "{\"symbols\":[],\"statements\":[[1,[18,\"infinite-scroller-extend\"],false]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template({
      "id": "HqndWukt",
      "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[4,\"infinite-scroller-extend\",null,null,{\"statements\":[[0,\"      template block text\\n\"]],\"parameters\":[]},null],[0,\"  \"]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), 'template block text');
  });
});
define('bbs/tests/integration/components/issue-list-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleForComponent)('issue-list', 'Integration | Component | issue list', {
    integration: true
  });

  (0, _emberQunit.test)('it renders', function (assert) {

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template({
      "id": "agyQXHRM",
      "block": "{\"symbols\":[],\"statements\":[[1,[18,\"issue-list\"],false]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template({
      "id": "Tk8ocH9D",
      "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[4,\"issue-list\",null,null,{\"statements\":[[0,\"      template block text\\n\"]],\"parameters\":[]},null],[0,\"  \"]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), 'template block text');
  });
});
define('bbs/tests/integration/components/navigation-bar-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleForComponent)('navigation-bar', 'Integration | Component | navigation bar', {
    integration: true
  });

  (0, _emberQunit.test)('it renders', function (assert) {

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template({
      "id": "pHYJxH4K",
      "block": "{\"symbols\":[],\"statements\":[[1,[18,\"navigation-bar\"],false]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template({
      "id": "SfqYzrT+",
      "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[4,\"navigation-bar\",null,null,{\"statements\":[[0,\"      template block text\\n\"]],\"parameters\":[]},null],[0,\"  \"]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), 'template block text');
  });
});
define('bbs/tests/integration/components/post-form-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleForComponent)('post-form', 'Integration | Component | post form', {
    integration: true
  });

  (0, _emberQunit.test)('it renders', function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template({
      "id": "TZWJ4cpN",
      "block": "{\"symbols\":[],\"statements\":[[1,[18,\"post-form\"],false]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template({
      "id": "1RHiBWBI",
      "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[4,\"post-form\",null,null,{\"statements\":[[0,\"      template block text\\n\"]],\"parameters\":[]},null],[0,\"  \"]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), 'template block text');
  });
});
define('bbs/tests/integration/components/post-item-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleForComponent)('post-item', 'Integration | Component | post item', {
    integration: true
  });

  (0, _emberQunit.test)('it renders', function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template({
      "id": "gUlChacF",
      "block": "{\"symbols\":[],\"statements\":[[1,[18,\"post-item\"],false]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template({
      "id": "QfOoOWWF",
      "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[4,\"post-item\",null,null,{\"statements\":[[0,\"      template block text\\n\"]],\"parameters\":[]},null],[0,\"  \"]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), 'template block text');
  });
});
define('bbs/tests/integration/components/user-list-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleForComponent)('user-list', 'Integration | Component | user list', {
    integration: true
  });

  (0, _emberQunit.test)('it renders', function (assert) {

    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.on('myAction', function(val) { ... });

    this.render(Ember.HTMLBars.template({
      "id": "OLDFidiJ",
      "block": "{\"symbols\":[],\"statements\":[[1,[18,\"user-list\"],false]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), '');

    // Template block usage:
    this.render(Ember.HTMLBars.template({
      "id": "0s87R978",
      "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[4,\"user-list\",null,null,{\"statements\":[[0,\"      template block text\\n\"]],\"parameters\":[]},null],[0,\"  \"]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), 'template block text');
  });
});
define('bbs/tests/integration/helpers/format-date-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleForComponent)('format-date', 'helper:format-date', {
    integration: true
  });

  // Replace this with your real tests.
  (0, _emberQunit.test)('it renders', function (assert) {
    this.set('inputValue', '1234');

    this.render(Ember.HTMLBars.template({
      "id": "wB2ngGss",
      "block": "{\"symbols\":[],\"statements\":[[1,[25,\"format-date\",[[19,0,[\"inputValue\"]]],null],false]],\"hasEval\":false}",
      "meta": {}
    }));

    assert.equal(this.$().text().trim(), '1234');
  });
});
define('bbs/tests/test-helper', ['bbs/tests/helpers/resolver', 'ember-qunit', 'ember-cli-qunit'], function (_resolver, _emberQunit, _emberCliQunit) {
  'use strict';

  (0, _emberQunit.setResolver)(_resolver.default);
  (0, _emberCliQunit.start)();
});
define('bbs/tests/tests.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | tests');

  QUnit.test('helpers/destroy-app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/destroy-app.js should pass ESLint\n\n');
  });

  QUnit.test('helpers/flash-message.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/flash-message.js should pass ESLint\n\n');
  });

  QUnit.test('helpers/module-for-acceptance.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/module-for-acceptance.js should pass ESLint\n\n');
  });

  QUnit.test('helpers/resolver.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/resolver.js should pass ESLint\n\n');
  });

  QUnit.test('helpers/start-app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/start-app.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/avatar-identicon-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/avatar-identicon-test.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/chat-window-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/chat-window-test.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/header-menus-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/header-menus-test.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/infinite-scroller-extend-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/infinite-scroller-extend-test.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/issue-list-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/issue-list-test.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/navigation-bar-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/navigation-bar-test.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/post-form-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/post-form-test.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/post-item-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/post-item-test.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/user-list-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/user-list-test.js should pass ESLint\n\n');
  });

  QUnit.test('integration/helpers/format-date-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/helpers/format-date-test.js should pass ESLint\n\n');
  });

  QUnit.test('test-helper.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'test-helper.js should pass ESLint\n\n');
  });

  QUnit.test('unit/adapters/application-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/adapters/application-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/adapters/users-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/adapters/users-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/bbs/index-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/bbs/index-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/issues/detail-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/issues/detail-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/login-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/login-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/user-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/user-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/users/new-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/users/new-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/contact-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/contact-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/login-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/login-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/user-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/user-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/about-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/about-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/bbs/index-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/bbs/index-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/bbs/read-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/bbs/read-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/contact-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/contact-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/index-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/index-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/issues/detail-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/issues/detail-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/login-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/login-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/posts-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/posts-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/serializers/post-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/serializers/post-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/services/flash-messages.js-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/services/flash-messages.js-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/services/post-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/services/post-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/services/reload-model-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/services/reload-model-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/services/send-post-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/services/send-post-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/views/upload-image-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/views/upload-image-test.js should pass ESLint\n\n');
  });
});
define('bbs/tests/unit/adapters/application-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('adapter:application', 'Unit | Adapter | application', {
    // Specify the other units that are required for this test.
    // needs: ['serializer:foo']
  });

  // Replace this with your real tests.
  (0, _emberQunit.test)('it exists', function (assert) {
    var adapter = this.subject();
    assert.ok(adapter);
  });
});
define('bbs/tests/unit/adapters/users-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('adapter:users', 'Unit | Adapter | users', {
    // Specify the other units that are required for this test.
    // needs: ['serializer:foo']
  });

  // Replace this with your real tests.
  (0, _emberQunit.test)('it exists', function (assert) {
    var adapter = this.subject();
    assert.ok(adapter);
  });
});
define('bbs/tests/unit/controllers/bbs/index-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('controller:bbs/index', 'Unit | Controller | bbs/index', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  // Replace this with your real tests.
  (0, _emberQunit.test)('it exists', function (assert) {
    var controller = this.subject();
    assert.ok(controller);
  });
});
define('bbs/tests/unit/controllers/issues/detail-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('controller:issues/detail', 'Unit | Controller | issues/detail', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  // Replace this with your real tests.
  (0, _emberQunit.test)('it exists', function (assert) {
    var controller = this.subject();
    assert.ok(controller);
  });
});
define('bbs/tests/unit/controllers/login-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('controller:login', 'Unit | Controller | login', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  // Replace this with your real tests.
  (0, _emberQunit.test)('it exists', function (assert) {
    var controller = this.subject();
    assert.ok(controller);
  });
});
define('bbs/tests/unit/controllers/user-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('controller:user', 'Unit | Controller | user', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  // Replace this with your real tests.
  (0, _emberQunit.test)('it exists', function (assert) {
    var controller = this.subject();
    assert.ok(controller);
  });
});
define('bbs/tests/unit/controllers/users/new-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('controller:users/new', 'Unit | Controller | users/new', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  // Replace this with your real tests.
  (0, _emberQunit.test)('it exists', function (assert) {
    var controller = this.subject();
    assert.ok(controller);
  });
});
define('bbs/tests/unit/models/contact-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleForModel)('contact', 'Unit | Model | contact', {
    // Specify the other units that are required for this test.
    needs: []
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var model = this.subject();
    // let store = this.store();
    assert.ok(!!model);
  });
});
define('bbs/tests/unit/models/login-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleForModel)('login', 'Unit | Model | login', {
    // Specify the other units that are required for this test.
    needs: []
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var model = this.subject();
    // let store = this.store();
    assert.ok(!!model);
  });
});
define('bbs/tests/unit/models/user-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleForModel)('user', 'Unit | Model | user', {
    // Specify the other units that are required for this test.
    needs: []
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var model = this.subject();
    // let store = this.store();
    assert.ok(!!model);
  });
});
define('bbs/tests/unit/routes/about-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('route:about', 'Unit | Route | about', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var route = this.subject();
    assert.ok(route);
  });
});
define('bbs/tests/unit/routes/bbs/index-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('route:bbs/index', 'Unit | Route | bbs/index', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var route = this.subject();
    assert.ok(route);
  });
});
define('bbs/tests/unit/routes/bbs/read-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('route:bbs/read', 'Unit | Route | bbs/read', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var route = this.subject();
    assert.ok(route);
  });
});
define('bbs/tests/unit/routes/contact-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('route:contact', 'Unit | Route | contact', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var route = this.subject();
    assert.ok(route);
  });
});
define('bbs/tests/unit/routes/index-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('route:index', 'Unit | Route | index', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var route = this.subject();
    assert.ok(route);
  });
});
define('bbs/tests/unit/routes/issues/detail-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('route:issues/detail', 'Unit | Route | issues/detail', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var route = this.subject();
    assert.ok(route);
  });
});
define('bbs/tests/unit/routes/login-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('route:login', 'Unit | Route | login', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var route = this.subject();
    assert.ok(route);
  });
});
define('bbs/tests/unit/routes/posts-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('route:posts', 'Unit | Route | posts', {
    // Specify the other units that are required for this test.
    // needs: ['controller:foo']
  });

  (0, _emberQunit.test)('it exists', function (assert) {
    var route = this.subject();
    assert.ok(route);
  });
});
define('bbs/tests/unit/serializers/post-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleForModel)('post', 'Unit | Serializer | post', {
    // Specify the other units that are required for this test.
    needs: ['serializer:post']
  });

  // Replace this with your real tests.
  (0, _emberQunit.test)('it serializes records', function (assert) {
    var record = this.subject();

    var serializedRecord = record.serialize();

    assert.ok(serializedRecord);
  });
});
define('bbs/tests/unit/services/flash-messages.js-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('service:flash-messages.js', 'Unit | Service | flash messages.js', {
    // Specify the other units that are required for this test.
    // needs: ['service:foo']
  });

  // Replace this with your real tests.
  (0, _emberQunit.test)('it exists', function (assert) {
    var service = this.subject();
    assert.ok(service);
  });
});
define('bbs/tests/unit/services/post-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('service:post', 'Unit | Service | post', {
    // Specify the other units that are required for this test.
    // needs: ['service:foo']
  });

  // Replace this with your real tests.
  (0, _emberQunit.test)('it exists', function (assert) {
    var service = this.subject();
    assert.ok(service);
  });
});
define('bbs/tests/unit/services/reload-model-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('service:reload-model', 'Unit | Service | reload model', {
    // Specify the other units that are required for this test.
    // needs: ['service:foo']
  });

  // Replace this with your real tests.
  (0, _emberQunit.test)('it exists', function (assert) {
    var service = this.subject();
    assert.ok(service);
  });
});
define('bbs/tests/unit/services/send-post-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('service:send-post', 'Unit | Service | send post', {
    // Specify the other units that are required for this test.
    // needs: ['service:foo']
  });

  // Replace this with your real tests.
  (0, _emberQunit.test)('it exists', function (assert) {
    var service = this.subject();
    assert.ok(service);
  });
});
define('bbs/tests/unit/views/upload-image-test', ['ember-qunit'], function (_emberQunit) {
  'use strict';

  (0, _emberQunit.moduleFor)('view:upload-image', 'Unit | View | upload image');

  // Replace this with your real tests.
  (0, _emberQunit.test)('it exists', function (assert) {
    var view = this.subject();
    assert.ok(view);
  });
});
require('bbs/tests/test-helper');
EmberENV.TESTS_FILE_LOADED = true;
//# sourceMappingURL=tests.map
