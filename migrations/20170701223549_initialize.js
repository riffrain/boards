
exports.up = function(knex, Promise) {
  return knex.schema
  .createTable('posts', function(t) {
    t.increments('id').primary();
    t.string('title').notNull();
    t.string('name');
    t.string('email');
    t.string('body').notNull();
    t.string('ip').notNull();
    t.string('ua').notNull();
    t.timestamps(true, true);
  })
  .createTable('replies', function(t) {
    t.increments('id').primary();
    t.integer('post_id', 10).unsigned().notNull().references('posts.id');
    t.integer('destination_id', 10).unsigned().references('replies.id');
    t.string('sort', 10);
    t.string('name');
    t.string('email');
    t.string('body').notNull();
    t.string('ip').notNull();
    t.string('ua').notNull();
    t.timestamps(true, true);
  })
  ;
};

exports.down = function(knex, Promise) {
  return knex.schema
    .dropTable('replies')
    .dropTable('posts')
  ;
};
