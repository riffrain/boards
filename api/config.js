/**
 * API configrations
 */
module.exports = {
  "db": {
    client:"mysql",
    connection: {
      host: "127.0.0.1",
      user: "app",
      password: "p@ssw0rd",
      database: "bbs",
      charset: "utf8mb4"
    }
  },
  "salt" : "itasecret"
};
