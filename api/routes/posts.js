var express = require("express");
var models = require("../models");
var Posts = require("../models/posts");
var Replies = require("../models/replies");
var _ = require('lodash');

var router = express.Router();

router.get("/", function(req, res, next) {
  var size = req.query.size;
  var page = req.query.page;
  if (size || page) {
    Posts.page({page, size})
      .then(function (posts) {
        res.send(posts);
      });
  } else {
    new Posts()
      .fetchAll()
      .then(function (posts) {
        res.send({"posts": posts.toJSON()});
      });
  }
});

router.get("/:id(\\d+)", function(req, res, next) {
  var postId = req.params.id;
  new Posts({id: postId})
    .fetch({
      withRelated: [{
        replies: function (query) {
          query
            .orderBy('post_id', 'ASC')
            .orderBy('destination_id', 'ASC')
            .orderBy('id', 'ASC');
        }
      }]
    })
    .then(function (post) {
      res.send({"post": post.toJSON()});
    });
});

router.post("/add", function(req, res, next) {
  var data = req.body;
  data.body = data.body || '';
  data.ip = req.headers['x-forwarded-for'];
  data.ua = req.headers['user-agent'];

  Posts
    .add(data)
    .then(function (post) {
      res.send(post);
    });
});

router.post("/add/:id(\\d+)", function(req, res, next) {
  var data = req.body;
  var postId = req.params.id;
  data.post_id = postId;
  data.ip = req.headers['x-forwarded-for'];
  data.ua = req.headers['user-agent'];

  Replies
    .add(data)
    .then(function (reply) {
      res.send(reply);
    });
});

module.exports = router;
