var express = require("express");
var helmet = require("helmet");
var bodyParser = require("body-parser");

var apiApp = express();
apiApp.use(helmet());

apiApp.use(bodyParser.json());
apiApp.use(bodyParser.urlencoded({ extended: false }));

apiApp.use("/posts", require("./routes/posts"));

module.exports = apiApp;
