let _         = require("lodash"),
    Bookshelf = require("bookshelf"),
    knex      = require("knex"),
    config    = require("../../config"),
    db,
    appBookshelf;

db = knex(config.db);
appBookshelf = Bookshelf(db);
appBookshelf.plugin("registry");

// enable fetchPage
appBookshelf.plugin("pagination");

module.exports = appBookshelf;
