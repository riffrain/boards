let Bookshelf = require("./base");
let Promise = require("bluebird");
let CheckIt = require("checkit");

let Reply = Bookshelf.Model.extend({
  tableName: "replies",
  initialize: function() {
    this.on('saving', this.validate, this);
  },
  validations: {
    body: ['required'],
  },
  validate: function(model, attrs, options) {
    return CheckIt(this.validations).run(this.toJSON());
  },
  post: function () {
    return this.belongsTo("Post", "post_id", "id");
  }
}, {
  add: Promise.method(function (data) {
    return new this(data)
      .save()
      .then(function (reply) {
        return reply.toJSON();
      })
      .catch(function(error) {
        if (error instanceof CheckIt.Error) {
          return {error: error.toJSON()};
        } else {
          return {error: error.message};
        }
      });
  }),
});

module.exports = Bookshelf.model("Reply", Reply);
