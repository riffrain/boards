let Bookshelf = require("./base");
let Promise = require("bluebird");
let CheckIt = require("checkit");
let Reply = require("../models/replies");

let Post = Bookshelf.Model.extend({
  tableName: "posts",
  initialize: function() {
    this.on('saving', this.validate, this);
  },
  validations: {
    title: ['required'],
    body: ['required']
  },
  validate: function(model, attrs, options) {
    return CheckIt(this.validations).run(this.toJSON());
  },
  replies: function () {
    return this.hasMany("Reply", "post_id");
  }
}, {
  page: async function (params) {
    return new this()
      .fetchPage({
        page: page,
        pageSize: pageSize
      })
      .then(function (post) {
        return {"posts": post.toJSON()};
      })
      .catch(function (error) {
        if (error instanceof CheckIt.Error) {
          return {error: error.toJSON()};
        } else {
          return {error: error.message};
        }
      });
  }),

  add: Promise.method(function (data) {
    return this.forge(data)
      .save()
      .then(function (post) {
        return post.toJSON();
      })
      .catch(function(error) {
        if (error instanceof CheckIt.Error) {
          return {error: error.toJSON()};
        } else {
          return {error: error.message};
        }
      });
  }),
});

module.exports = Bookshelf.model("Post", Post);
