// Update with your config settings.

module.exports = {

  development: {
    client: 'mysql',
    connection: {
      host : '127.0.0.1',
      user : 'app',
      password : 'p@ssw0rd',
      database : 'bbs'
    },
    migrations: {
      directory: 'migrations',
      tableName: 'knex_migrations'
    }
  },

  staging: {
    client: 'mysql',
    connection: {
      host : '127.0.0.1',
      user : 'app',
      password : 'p@ssw0rd',
      database : 'bbs'
    },
    migrations: {
      directory: 'migrations',
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'mysql',
    connection: {
      host : '127.0.0.1',
      user : 'app',
      password : 'p@ssw0rd',
      database : 'bbs'
    },
    migrations: {
      directory: 'migrations',
      tableName: 'knex_migrations'
    }
  }

};
