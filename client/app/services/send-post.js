import Ember from 'ember';

export default Ember.Service.extend({
  post (url, data) {
    return new Promise(function (resolve, reject) {
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      })
      .then(function (res) {
        return res.json();
      })
      .then(function (json) {
        resolve(json);
      })
      .catch(function (error) {
        reject(error);
      });
    });
  },
});
