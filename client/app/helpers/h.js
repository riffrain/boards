import Ember from 'ember';
import caja from '../utils/sanitize';

// google-caja sanitizer wrapper
export function h(params) {
  return caja.sanitize(params[0]).replace(/\r?\n/g, '<br>');
}

export default Ember.Helper.helper(h);
