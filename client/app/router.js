import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('not-found', {path: '/*path'});
  this.route('index', {path: '/'});
  this.route('bbs', function (){
    this.route('index', {path: '/'});
    this.route('read', {path: '/read/:id'});
  });
});

export default Router;
