import Ember from 'ember';

export default Ember.Component.extend({
  isReply: false,

  name: function () {
    if (!this.get('item.name')) {
      return 'Anonymous';
    } else {
      return this.get('item.name');
    }
  }.property('item'),

  padId: function () {
    return (this.get('item.id') || '').padStart(10, '0');
  }.property('item'),

  actions: {
    reply: function () {
      this.toggleProperty('isReply');
    },
    completeSend: function (data) {
      if (data.error) {
        alert(data.error.body);
      }
      this.get('modelReload')();
    }
  }
});
