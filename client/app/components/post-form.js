import Ember from 'ember';

export default Ember.Component.extend({
  sendPost: Ember.inject.service('send-post'),

  enableTitle: Ember.computed('type', function() {
    if (this.get('type') === 'post') {
      return true;
    } else {
      return false;
    }
  }),

  getUrl: function () {
    if (this.get('type') === 'reply') {
      return '/api/posts/add/' + this.get('postId');
    } else {
      return '/api/posts/add/';
    }
  },

  beforeSubmit: function () {
    this.set('isPosting', true);
  },
  afterSubmit: function () {
    this.set('isPosting', false);

    this.set('title', null);
    this.set('name', null);
    this.set('email', null);
    this.set('body', null);

  },
  completeSubmit: function (data) {
    // nothing
  },

  actions: {
    onSubmit: function () {
      let self = this;
      let data = self.getProperties('name', 'email', 'body');

      if (this.get('type') !== 'reply') {
        data.title = this.get('title');
      }
      if (this.get('replyId')) {
        data.destination_id = this.get('replyId');
      }
      let url = this.getUrl();

      self.beforeSubmit();

      self.get('sendPost.post')(url, data)
        .then(function (data) {
          self.afterSubmit();
          return data;
        })
        .then(function (data) {
          self.completeSubmit(data);
        });

    }
  }
});
