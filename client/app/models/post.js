import DS from 'ember-data';

export default DS.Model.extend({
  title : DS.attr(),
  name  : DS.attr(),
  email : DS.attr(),
  body  : DS.attr(),
  created_at  : DS.attr(),
  replies: DS.hasMany('reply')
});
