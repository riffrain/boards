import DS from 'ember-data';

export default DS.Model.extend({
  name  : DS.attr(),
  email : DS.attr(),
  body  : DS.attr(),
  post_id  : DS.attr(),
  destination_id  : DS.attr(),
  created_at  : DS.attr()
});
