import Ember from 'ember';
import EmberObject, { computed } from '@ember/object';

export default Ember.Controller.extend({
  sendPost: Ember.inject.service('send-post'),

  actions: {
    reload: function () {
      return this.send('modelReload');
    },
    completeSend: function (data) {
      if (data.error) {
        alert(data.error.body);
      }
      this.send('reload');
    },
  }
});
