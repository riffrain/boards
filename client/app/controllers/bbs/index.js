import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    completeSend: function (data) {
      if (data.error) {
        let message = '';
        Object.keys(data.error).forEach(function (key) {
          message += data.error[key] + '\n';
        });
        alert(message);
      } else {
        this.transitionToRoute('bbs.read', data.id);
      }
    }
  }
});
