import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    //return this.get('store').query('post', {size: 20});
    return this.get('store').findAll('post');
  },
  setupController(controller, model) {
    this._super(...arguments);
  },
  actions: {
    modelReload: function () {
      this.refresh();
    }
  }
});
