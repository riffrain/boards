import Ember from 'ember';

export default Ember.Route.extend({
  model(params) {
    this.set('postId', params.id);
    return this.get('store').find('post', params.id);
  },
  setupController(controller, model) {
    this._super(...arguments);
    controller.set('postId', this.get('postId'));
  },
  actions: {
    modelReload: function () {
      this.refresh();
    }
  }
});
